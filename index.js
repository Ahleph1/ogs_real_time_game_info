var request = require('request');
var exec = require('child_process').exec;
var http = require('http');
var express = require('express');
var io = require('socket.io')(http);
var path = require('path');
var assert = require('assert');
var fs = require('fs');
var Game = require("tenuki").Game;
var token = ""

var app = express();
app.use(express.static('.'));
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'views', 'index.html'));
});

io.sockets.on('connection', function (socket){
    refreshToken();
    // token to use OGS API has a time limit. this function is called every few hours to refresh the token
    setInterval(refreshToken, 21600000);
    socket.on("gameID", function(gameID) {
        try{
            clearInterval(gameUpdater);
        }
        catch(err){
            null;
        }
        gameUpdater = setInterval(updateGame, 1000, socket, gameID);
    });
    socket.on("get ranks", function(player_ids) {
        console.log(player_ids);
        rank = [null, null]
        cmd = "curl -H \"Authorization: Bearer "+token + "\"" + " https://online-go.com/api/v1/players/" + player_ids[0].toString()
        exec(cmd, function(error, stdout, stderr) {
            console.log(stdout);
            playerInfo = JSON.parse(stdout);
            rank[0] = playerInfo["ranking"];
            command = "curl -H \"Authorization: Bearer "+token + "\"" + " https://online-go.com/api/v1/players/" + player_ids[1].toString()
            exec(command, function(error, stdout, stderr) {
                console.log(stdout);
                playerInfo = JSON.parse(stdout);
                rank[1] = playerInfo["ranking"];
                socket.emit("get player ranks", rank);
            });
        });
    })
});

function refreshToken() {
    request({
    	method: 'POST',
    	url: 'https://online-go.com/oauth2/access_token',
    	headers: {
        	'Content-Type': 'application/x-www-form-urlencoded'
    	},
    	body: "client_id=REDACTED&client_secret=REDACTED&grant_type=password&username=REDACTED&password=REDACTED"
    	}, function (error, response, body) {
        	token = JSON.parse(body)["access_token"];
          console.log(token);
    });
}

function updateGame(socket, gameID) {
    console.log("is this thing on?");
    cmd = "curl -H \"Authorization: Bearer \""+token + " https://online-go.com/api/v1/games/" + gameID
    exec(cmd, function(error, stdout, stderr) {
        // set up tenuki.js game engine to record moves to count captures
        game = new Game();
        game.setup();
        // add all moves to tenuki.js

        gameData = JSON.parse(stdout);
        gameInfo = {
            "initial-game-time" : gameData["gamedata"]["time_control"]["initial_time"],
            "moves" : gameData["gamedata"]["moves"],

            "player-turn-id" : gameData["gamedata"]["clock"]["current_player"],
            "last-move-time" : gameData["gamedata"]["clock"]["last_move"],
            "current-time" : (new Date).getTime(),
            "byo-yomi" : gameData["gamedata"]["clock"]["black_time"]["period_time"],
            "byo-yomi-periods" : 100,

            "white-id" : gameData["players"]["white"]["id"],
            "white-name" : gameData["players"]["white"]["username"],
            // captures counted from tenuki.js, added below
            "white-captured" : '',
            "white-thinking-time" : gameData["gamedata"]["clock"]["white_time"]["thinking_time"],
            "white-byo-yomi-periods-left" : gameData["gamedata"]["clock"]["white_time"]["periods"],

            "black-id" : gameData["players"]["black"]["id"],
            "black-name" : gameData["players"]["black"]["username"],
            "black-captured" : '',
            "black-thinking-time" : gameData["gamedata"]["clock"]["black_time"]["thinking_time"],
            "black-byo-yomi-periods-left" : gameData["gamedata"]["clock"]["black_time"]["periods"]
        }

        for (var i=0; i<gameInfo["moves"].length; i++) {
	          try{
                game.playAt(gameInfo["moves"][i][0], gameInfo["moves"][i][1]);
            }
	          catch(err){
		            console.log(err);
	          }
	      }
	      try {
            gameInfo["white-captured"] = game["_moves"][game["_moves"].length - 1]["whiteStonesCaptured"]
            gameInfo["black-captured"] = game["_moves"][game["_moves"].length - 1]["blackStonesCaptured"]
        } catch(err) {
            console.log(err);
	      }
        if (gameData["ended"] != null) {
            socket.emit("game over", gameInfo);
            clearInterval(gameUpdater);
        } else {
            socket.emit("bab update", gameInfo);
        }
    });
}

io.listen(app.listen(3000));
