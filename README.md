# Overview

Shows OGS game information in a clean, compact way for video streamers to use instead of the ugly OGS interface.

Example (actual game) showing player name, rank, captures, byo yomi info, and time remaining:

![alt text](example.png "OGS Example")

# Getting Started

### After cloning the repo, before running node index.js you will need OGS API credentials.

Go to https://online-go.com/developer, login (or register) and get a client ID and client secret.
Then, on your OGS settings page, add an application specific password.

Open index.js with your favorite editor and, in the refreshToken function, replace "REDACTED" with appropriate values in 4 cases based on the instructions above.

Run ```npm install``` then ```node index.js```. Then go to http://localhost:3000.

Enter the ID of a game you wish to track (the numbers after the URL when spectating an OGS game).

###### Example for actual game:

https://online-go.com/5891997

Enter 5891997 into the textbox and click Submit. Tracked until either a new game ID is entered
and submit is clicked again (where the process starts over) or the game finishes.
