window.onload = function() {
    var ranks = '';
    socket = io.connect('http://localhost:3000', {'force new connection': true});
    socket.on('connect', function(){
        moveArray = new Array(19);
        for (var i=0; i<19; i++) {
            moveArray[i] = new Array(19);
        }
        socket.on("bab update", function(gameInfo) {
            whitePlayerName = document.getElementById("white-player-name");
            whiteCaptures = document.getElementById("white-player-captures");
            whiteByoYomi = document.getElementById("white-player-byo-yomi");
            whitePlayerTimer = document.getElementById("white-player-timer");

            blackPlayerName = document.getElementById("black-player-name");
            blackCaptures = document.getElementById("black-player-captures");
            blackByoYomi = document.getElementById("black-player-byo-yomi");
            blackPlayerTimer = document.getElementById("black-player-timer");

            if (ranks != '') {
                whitePlayerName.innerHTML = "&nbsp;&nbsp;" + gameInfo["white-name"] + " " + ranks[0];
                blackPlayerName.innerHTML = "&nbsp;&nbsp;" + gameInfo["black-name"] + " " + ranks[1];
            } else {
                whitePlayerName.innerHTML = "&nbsp;&nbsp;" + gameInfo["white-name"];
                blackPlayerName.innerHTML = "&nbsp;&nbsp;" + gameInfo["black-name"];
            }

            whiteByoYomi.innerHTML = gameInfo["byo-yomi"] + " sec / " + gameInfo["white-byo-yomi-periods-left"];
            whiteCaptures.innerHTML = gameInfo["black-captured"];
            blackByoYomi.innerHTML = gameInfo["byo-yomi"] + " sec / " + gameInfo["black-byo-yomi-periods-left"];
            blackCaptures.innerHTML = gameInfo["white-captured"];

            if (ranks == '') {
                socket.emit("get ranks", [gameInfo["white-id"], gameInfo["black-id"]])
            }

            setPlayerTimers(whitePlayerTimer, blackPlayerTimer, gameInfo);

        })
        socket.on("game over", function(gameInfo) {
          ranks = '';
          whitePlayerName = document.getElementById("white-player-name");
          whiteCaptures = document.getElementById("white-player-captures");
          whiteByoYomi = document.getElementById("white-player-byo-yomi");
          whitePlayerTimer = document.getElementById("white-player-timer");

          blackPlayerName = document.getElementById("black-player-name");
          blackCaptures = document.getElementById("black-player-captures");
          blackByoYomi = document.getElementById("black-player-byo-yomi");
          blackPlayerTimer = document.getElementById("black-player-timer");

          whitePlayerName.innerHTML = "&nbsp;&nbsp;" + gameInfo["white-name"];
          whiteCaptures.innerHTML = "";
          whiteByoYomi.innerHTML = "";
          whitePlayerTimer.innerHTML = "";
          blackPlayerName.innerHTML = "&nbsp;&nbsp;" + gameInfo["black-name"];
          blackByoYomi.innerHTML = "";
          blackCaptures.innerHTML = "";
          blackPlayerTimer.innerHTML = "";
        })

        socket.on("get player ranks", function(ranks_from_server) {
            ranks = [ranks_from_server[0], ranks_from_server[1]]
            if (ranks[0] < 30) {
                ranks[0] = "[" + (30-ranks[0]) + "k" + "]"
            } else {
                ranks[0] = "[" + (ranks[0] - 29) + "d" + "]"
            }
            if (ranks[1] < 30) {
                ranks[1] = "[" + (30-ranks[1]) + "k" + "]"
            } else {
                ranks[1] = "[" + (ranks[1] - 29) + "d" + "]"
            }
        })

    });

    var submitButton = document.getElementById("submit")
    submitButton.onclick = function(){
	      ranks = ''
        socket.emit("gameID", document.getElementById("game id").value);
    }
}

function setPlayerTimers(whitePlayerTimer, blackPlayerTimer, gameInfo) {
    if (gameInfo["player-turn-id"] === gameInfo["white-id"]) {
        if (gameInfo["white-thinking-time"] - (gameInfo["current-time"] - gameInfo["last-move-time"])/1000 > 0) {
            whitePlayerTimer.innerHTML = parseSecondsToReadable(gameInfo["white-thinking-time"] - (gameInfo["current-time"] - gameInfo["last-move-time"])/1000)
        } else {
            maxByoYomiTime = gameInfo["byo-yomi"] * gameInfo["byo-yomi-periods"]
            whitePlayerTimer.innerHTML = parseSecondsToReadable((gameInfo["white-thinking-time"] + maxByoYomiTime - (gameInfo["current-time"] - gameInfo["last-move-time"])/1000)%gameInfo["byo-yomi"]+1)
        }
    } else {
      if (gameInfo["black-thinking-time"] - (gameInfo["current-time"] - gameInfo["last-move-time"])/1000 > 0) {
          blackPlayerTimer.innerHTML = parseSecondsToReadable(gameInfo["black-thinking-time"] - (gameInfo["current-time"] - gameInfo["last-move-time"])/1000)
      } else {
          maxByoYomiTime = gameInfo["byo-yomi"] * gameInfo["byo-yomi-periods"]
          blackPlayerTimer.innerHTML = parseSecondsToReadable((gameInfo["black-thinking-time"] + maxByoYomiTime - (gameInfo["current-time"] - gameInfo["last-move-time"])/1000)%gameInfo["byo-yomi"]+1)
      }
    }
}

function parseMillisecondsToReadable(timeInMilliseconds) {
    return parseSecondsToReadable(Math.floor(timeInMilliseconds / 1000))
}

function parseSecondsToReadable(timeInSeconds) {
    timeInSeconds = parseInt(timeInSeconds);
    minutes = Math.floor(timeInSeconds / 60);
    seconds = Math.floor(timeInSeconds - 60*minutes);
    if (seconds < 10) {
        seconds = "0" + seconds.toString();
    }
    return minutes.toString() + ":" + seconds
}
